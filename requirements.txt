Flask==1.1.1
requests==2.22.0
numpy==1.20.2
pandas==1.2.4
pandasql==0.7.3
xlrd==1.2.0